//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Command*/	/*Update Interval*/	/*Update Signal*/
	/*	{" ",   "$HOME/.local/scripts/statusbar/music",	            4,	  16}, */
	{"  ",   "$HOME/.local/scripts/statusbar/torrent",	             10,	   13},
	{"  ",   "$HOME/.local/scripts/statusbar/mailbox",	             60,    14},
	{"  ",   "$HOME/.local/scripts/statusbar/pacupdate",	         1800,  12},
	{"  \uf0d6 ", "$HOME/.local/scripts/statusbar/crypto_total",     600,   10},
	{"  \uf218 ", "$HOME/.local/scripts/statusbar/crypto_sft_total", 600,   11},
	{"  \uf02d ", "$HOME/.local/scripts/statusbar/crypto_lbc",	     600,   19},
	{"  ", "$HOME/.local/scripts/statusbar/disk-root",     800,    9},
	{"  \uf0a0 ", "$HOME/.local/scripts/statusbar/disk-home",    800,    8},
	/*	{" ",   "$HOME/.local/scripts/statusbar/disk-10tb",	        800,	7}, */
	/*{"  \uf06d ",  "$HOME/.local/scripts/statusbar/gpu",		    10,	    6}, */
	{"  \uf2c8 ",  "$HOME/.local/scripts/statusbar/cpu",		    5,	    5},
	{"  \uf108 ",  "$HOME/.local/scripts/statusbar/memory",	        10,	    4},
	{"  ",    "$HOME/.local/scripts/statusbar/moonphase",	        1800,   2},
	{"  \uf0e9 ",    "$HOME/.local/scripts/statusbar/weather1",	    1800,   3},
	{"  \uf2cb ",    "$HOME/.local/scripts/statusbar/weather2",	    1800,   3},
	{"  \uf073 ",  "$HOME/.local/scripts/statusbar/calender",	    1800,   1},
    {"  \uf017 ",  "$HOME/.local/scripts/statusbar/clock",	        60,     1},
	{"  ",   "$HOME/.local/scripts/statusbar/iplocate",	            5,     15},
    {" ",  "",	       0,    0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim = '\0';

// Have dwmblocks automatically recompile and run when you edit this file in
// vim with the following line in your vimrc/init.vim:

// autocmd BufWritePost */dwmblocks/config.h !sudo make install && { killall -q dwmblocks;setsid dwmblocks & }

